const bcrypt = require("bcrypt");
const Joi = require("joi");
const express = require("express");
const router = express.Router();
const User = require("../models/user");

router
  .get("/", (req, res) => {
    User.find().then(users => res.send(users))
  })
  .post("/", async (req, res) => {

    const checkUser = await User.findOne({ email: req.body.email })
    console.log(checkUser);
    if (checkUser) return res.send('user already exsists!')

    const {error} = validateUser(req.body)
    if( error ) return res.send(error.details[0].message)


    let newUser = new User({
      ...req.body
    });

    const salt = await bcrypt.genSalt(10);
    newUser.password = await bcrypt.hash(newUser.password, salt);
    await newUser.save()
    res.send(newUser)
  });


validateUser = user => {
  const schema = {
    //customerId: Joi.objectId().required(),
    email: Joi.required(),
    password: Joi.string()
      .min(5)
      .max(16)
      .required()
  };

  return Joi.validate(user, schema);
};


module.exports = router