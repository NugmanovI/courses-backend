const express = require("express");
const router = express.Router();
const Course = require("../models/course");
const uuid = require("uuid");

router
  .get("/", (req, res) => {
    Course.find().then(courses => res.send(courses))
  })
  .get("/:id", (req, res) => {
    let { id } = req.params;
    Course.findById(id).then(result => res.send(result));
  })

  .post("/", (req, res) => {
    let { title, price } = req.body;
    let newCourse = new Course({
      title,
      price
    });

    newCourse.save().then(result => res.send(result));
  })

  .put("/:id", (req, res) => {
    let { id } = req.params;
    let changes = req.body;

    // const obj = {
    //   ...req.body
    // }

    Course.findByIdAndUpdate(id, { $set: changes }, { new: true }).then(result =>
      res.status(200).send('Course saved')
    );
  })

  .delete("/:id", (req, res) => {
    let { id } = req.params;
    // Course.findOneAndRemove({ _id: id });
    Course.findByIdAndDelete(id)
      .then(res.send("Successfully deleted!"))
      .catch(e => console.log(e.message));
  });

module.exports = router;
