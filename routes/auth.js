const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("../models/user");

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ email });
  if (!user) return res.status(400).send("Password or email is incorrect");

  const correctPass = await bcrypt.compare(password, user.password);
  if (!correctPass)
    return res.status(400).send("Password or email is incorrect");

    const token = user.generateToken()

    res.header("x-user-toke", token).send()

});

module.exports = router;
