const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const jwt = require("jsonwebtoken");

const userSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  }
});

userSchema.methods.generateToken = function() {
  const token = jwt.sign({ _id: this._id }, 'jwttoken')
  return token;
}
module.exports = User = mongoose.model("User", userSchema);
