const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const courseSchema = new Schema({
  id:  {
    type: Number
  },
  title:  {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  author: {
    type: String,
    default: 'admin'
  },
  date: { type: Date, default: Date.now },
  price: {
    type: Number,
    required: true
  },
  isCompleted: {
    type: Boolean,
    default: false
  }
});

module.exports = Course = mongoose.model('Course', courseSchema);
