const express = require('express');
const app = express();

const mongoose = require('mongoose');
const courses = require('./routes/courses');
const users = require("./routes/users");
const auth = require("./routes/auth");
const bodyParser = require('body-parser')

mongoose.connect('mongodb+srv://Izaku:password355@cluster0-pjt3c.mongodb.net/test?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use('/api/courses', courses);
app.use("/api/users", users);
app.use("/api/auth", auth);


const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`App listening port ${port}`));
